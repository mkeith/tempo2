This is the OLD tempo2 repo. Please run 

    ./update

to update!

Or change your git settings manually:

    git remote set-url origin https://bitbucket.org/psrsoft/tempo2.git

Developers should use:

    git remote set-url origin git@bitbucket.org:psrsoft/tempo2.git